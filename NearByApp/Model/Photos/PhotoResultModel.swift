//
//  PhotoResultModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//


import ObjectMapper

class PhotoResultModel: Mappable {
    
    var meta:MetaResponse?
    var response:PhotoResponseModel?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        meta                 <- map["meta"]
        response            <- map["response"]
    }
}
