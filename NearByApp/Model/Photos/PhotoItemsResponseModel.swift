//
//  PhotosResponseModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class PhotoItemsResponseModel: Mappable {
    
    var items:[ItemResponseModel]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        items          <- map["items"]
    }
}
