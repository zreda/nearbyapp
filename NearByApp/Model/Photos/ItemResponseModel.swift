//
//  PhotoItemResponseModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class ItemResponseModel: Mappable {
    var prefix:String?
    var suffix:String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        prefix            <- map["prefix"]
        suffix            <- map["suffix"]
    }
}
