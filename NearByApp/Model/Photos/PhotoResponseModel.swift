//
//  PhotoResultModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class PhotoResponseModel: Mappable {
    
    var photos:PhotoItemsResponseModel?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        photos          <- map["photos"]
    }
}
