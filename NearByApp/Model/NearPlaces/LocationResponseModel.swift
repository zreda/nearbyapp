//
//  LocationReponseModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//


import ObjectMapper

class LocationResponseModel: Mappable {
    
    var address:String?
    var formattedAddress:[String]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        address                     <- map["address"]
        formattedAddress            <- map["formattedAddress"]
    }
}
