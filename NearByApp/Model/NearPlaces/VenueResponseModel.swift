//
//  VenueReponseModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class VenueResponseModel: Mappable {
    
    var id:String?
    var name:String?
    var location:LocationResponseModel?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id                   <- map["id"]
        name                 <- map["name"]
        location             <- map["location"]
    }
}
