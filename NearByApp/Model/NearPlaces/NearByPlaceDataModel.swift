//
//  NearByPlaceDataModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class NearByPlaceDataModel: NSObject , NSCoding {
    
    var name:String?
    var address:String?
    var imgUrl:String?
    var id:String?
    var img:NSData?
    
    
    override init() {
        self.name = ""
        self.address = ""
        self.imgUrl = ""
    }
    
    init(name:String ,address:String ,imgUrl:String) {
        self.name = name
        self.address = address
        self.imgUrl = imgUrl
    }
    
    init(name:String ,address:String ,imgUrl:String,id:String) {
        self.name = name
        self.address = address
        self.imgUrl = imgUrl
        self.id = id

    }
    
    init(name:String ,address:String )
    {
        self.name = name
        self.address = address
        self.imgUrl = ""
    }
    
    
    
    required init(coder decoder: NSCoder) {
        
        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
        self.address = decoder.decodeObject(forKey: "address") as? String ?? ""
        self.imgUrl = decoder.decodeObject(forKey: "imgUrl") as? String ?? ""

    }
    
    func encode(with coder: NSCoder) {
        
        coder.encode(name ,forKey: "name")
        coder.encode(address ,forKey: "address")
        coder.encode(imgUrl ,forKey: "imgUrl")
        
    }
    
    
}
