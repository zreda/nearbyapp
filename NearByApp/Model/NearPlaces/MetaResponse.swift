//
//  MetaReponse.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class MetaResponse: Mappable {
    var code:Int?
    var requestId:String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        code                 <- map["code"]
        requestId            <- map["requestId"]
    }
}

