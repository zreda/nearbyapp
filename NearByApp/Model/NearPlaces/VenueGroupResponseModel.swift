//
//  VenueGroupResponseModel.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class VenueGroupResponseModel: Mappable {
    
    var items:[VenueItemsResponseModel]?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        items                 <- map["items"]
    }
}
