//
//  NearByPlacesViewController.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import CoreLocation
import ReachabilitySwift

/*
 
 location state 
    
    - Realtime : for every update location after specfic meter
    - Single   : for get current location only launching
 
 */


 enum LocationMode : String

{
    case Realtime = "Realtime"
    case Single = "Single Update"
}
class NearByPlacesViewController: UIViewController {
    
    @IBOutlet fileprivate weak var placesTB: UITableView!
    @IBOutlet fileprivate weak var modeBtn: UIBarButtonItem!
    
    fileprivate var nearByPlacesPresenter: NearByPlacesPresenter?
    
    fileprivate var nearByPlaces:[NearByPlaceDataModel] = []
    fileprivate var locationManager: CLLocationManager = CLLocationManager()
    fileprivate var currLoc: CLLocationCoordinate2D?
    fileprivate var mode:LocationMode?
    
    let reachability = Reachability()

    
    override func viewDidLoad() {
        
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        reachability?.whenUnreachable = {
            reachability in
            
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kInternetConnectionNotReachable), object: nil)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability?.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getCachedPlaces), name: Notification.Name(kInternetConnectionNotReachable), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUpdatedNearbyPlace), name: Notification.Name(kInternetConnectionReachable), object: nil)


        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    /*
 
        initiate views and check if there is no connection get cached data
     
     */
    func initView()
    {
        placesTB?.tableFooterView = UIView(frame: CGRect.zero)
        placesTB.rowHeight = UITableViewAutomaticDimension
        placesTB.estimatedRowHeight = Constants.rowHeight
    
        if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.locationMode)

        {
            mode = (Helper.getUserDefault(key: Constants.userDefault.locationMode) as? String).map { LocationMode(rawValue: $0) }!
            modeBtn.title = mode.map { $0.rawValue }
            
            
        }
        else
        {
            mode = LocationMode.Realtime
            modeBtn.title = LocationMode.Single.rawValue

            
        }
        locationManager.delegate = self
        
    }
    
    
    /*
     
     change between location modes real time and single time
     
     */
    @IBAction func changeModeBtnTapped(_ sender: Any) {
        
        
        removeLabelNoData()
        
        
        if mode == .Realtime
        {
        
            self.modeBtn.title = LocationMode.Realtime.rawValue
            Helper.saveUserDefault(key: Constants.userDefault.locationMode, value: LocationMode.Single.rawValue)
            mode = .Single
            locationManager.stopUpdatingLocation()

        }
        else
        {
        
            self.modeBtn.title = LocationMode.Single.rawValue
            Helper.saveUserDefault(key: Constants.userDefault.locationMode, value: LocationMode.Realtime.rawValue)
            mode = .Realtime
            locationManager.startUpdatingLocation()

        }
        
        locationManager.delegate = self

    }
}



extension NearByPlacesViewController :UITableViewDelegate , UITableViewDataSource , NearByPlacesView ,  CLLocationManagerDelegate
{
    
    /*
        set near by places whatever realtime or single time
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = placesTB.dequeueReusableCell(withIdentifier: "place_cell", for: indexPath) as! PlaceTableViewcell
        
        if nearByPlaces[indexPath.row].imgUrl == ""
        {
            
            nearByPlacesPresenter?.getPlaceImage(photoId: nearByPlaces[indexPath.row].id ?? "", index: indexPath.row)
        }
        
        cell.configureCell(model: nearByPlaces[indexPath.row])
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return nearByPlaces.count
        
    }
    
   
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    
    /*
     
        delegate to get  data from foursquare
     
     */
    func setPlaceList(_ places: [NearByPlaceDataModel]?) {
        
        
        self.nearByPlaces = places!
        Helper.saveObjectDefault(key: Constants.userDefault.placesData, value: places!)
        self.placesTB.reloadData()
        
    }
    
    func setEmptyList() {
        
        
        addLabelNoData(noData: "No data found !!")
    }
    
    func setError() {
        
        addLabelNoData(noData: "something went wrong !!")
       
    }
    
    func setPlaceImage(imgUrl: String , index:Int) {
        
        nearByPlaces[index].imgUrl = imgUrl
        
       if (placesTB.cellForRow(at: IndexPath(row: index, section: 0)) as? PlaceTableViewcell) != nil
       {
            placesTB.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        
        }
        
    }
    
  
    // MARK: - Location Manager
    
    func enableLocationService() {
        
        
        if CLLocationManager.locationServicesEnabled() {
            
                locationManager.requestWhenInUseAuthorization()
                locationManager.distanceFilter = 50
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startMonitoringSignificantLocationChanges()
                locationManager.startUpdatingLocation()
            
        } else {
            
            addLabelNoData(noData: "Location not Allowed !!")
            
            print("Location services are not enabled")
        }
        
        
        
        
    }
    
    
    // MARK: - Location Manager Delegate 
    /*
     
      get current location for user to get surrounded places based on his location for 
     
     -  real time : every 500 meter
     - single time : when launching application
     
     */
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        if currLoc == nil
        {
            currLoc = manager.location!.coordinate
        }

        let lastLocCoordinate = CLLocation(latitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
        
        let currLocCoordinate = CLLocation(latitude: (currLoc?.latitude)!, longitude: (currLoc?.longitude)!)

        if lastLocCoordinate.distance(from: currLocCoordinate) > 500.0
        {
            currLoc = manager.location!.coordinate
            if NetworkUtil.getNetworkStatus() {
                self.nearByPlacesPresenter = NearByPlacesPresenter(self)
                self.nearByPlacesPresenter?.getPlaceListResponse(lat: (currLoc?.latitude)!, long: (currLoc?.longitude)!)
            }
        
        }
        else
        {
           if  nearByPlaces.count == 0
           {
            
            if NetworkUtil.getNetworkStatus() {
                self.nearByPlacesPresenter = NearByPlacesPresenter(self)
                self.nearByPlacesPresenter?.getPlaceListResponse(lat: (currLoc?.latitude)!, long: (currLoc?.longitude)!)
            }

            }
        }
        
        if self.mode == .Single
        {
            locationManager.stopUpdatingLocation()
            
            
        }
        
        

    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .denied
        {
            enableLocationService()
            
        }
        
    }
    
    
    
    /*
     
      listenting for updating network status 
        - enable : get near by location 
        - disabled : get latest retrieved near by places 
     
     */
    func reachabilityChanged(note: NSNotification) {
        
        
        if NetworkUtil.getNetworkStatus() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kInternetConnectionReachable), object: nil)
        } else {
            print("Network not reachable")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kInternetConnectionNotReachable), object: nil)
        }
    }
    
    
    func getCachedPlaces()  {

        if Helper.isKeyPresentInUserDefaults(key: Constants.userDefault.placesData)
        {
            nearByPlaces = Helper.getObjectDefault(key: Constants.userDefault.placesData) as! [NearByPlaceDataModel]
        }

    }
    
    
    func getUpdatedNearbyPlace()  {

        self.enableLocationService()

    
    }
    
    
    
}
