import MBProgressHUD
    
extension UIViewController
{
    
    func addLabelNoData(noData:String)  {
        let label = UILabel(frame: CGRect.zero)
        label.text = noData
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.tag = 100
       // label.backgroundColor = .red  // Set background color to see if label is centered
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        
        let widthConstraint = NSLayoutConstraint(item: label, attribute: .width, relatedBy: .equal,toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 250)
        
        let heightConstraint = NSLayoutConstraint(item: label, attribute: .height, relatedBy: .equal,toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
        
        let xConstraint = NSLayoutConstraint(item: label, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: label, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstraint, yConstraint])
    }
    func removeLabelNoData()  {
        self.view.viewWithTag(100)?.removeFromSuperview()
    }
    
    
    func showGlobalProgressHUDWithTitle(view:UIView!) -> MBProgressHUD{
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        
        
        if title == nil{
            hud.label.text = NSLocalizedString("Loading ... ", comment: "Loading")
        }else{
            hud.label.text = title!
        }
        return hud
    }

}

