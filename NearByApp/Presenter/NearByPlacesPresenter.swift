//
//  NearByPlacesPresenter.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

protocol NearByPlacesView: NSObjectProtocol {
    
    func setPlaceList(_ places: [NearByPlaceDataModel]?)
    func setPlaceImage(imgUrl:String , index:Int)
    func setEmptyList()
    func setError()
}

class NearByPlacesPresenter {
    fileprivate let exploreService = NearPlacesManager()
    weak fileprivate var nearByPlacesView : NearByPlacesView?
    
    
    init(_ view:NearByPlacesView){
        nearByPlacesView = view
    }
    
    init()
    {
        
    }
    
    
    func detachView() {
        nearByPlacesView = nil
    }
    
    public func getPlaceListResponse(lat:Double,long:Double){
        
        
        let result = Helper.getCurrentDate(format: "yyyyMMdd")
        
        exploreService.expolreVenues(lat: String(lat), lng: String(long), version: result, client_id: getFoursquareKey().0, client_secret: getFoursquareKey().1) { (response, error) in
            
            if error == nil
            {
                if response?.meta?.code == 200
                {
                    
                    if response?.response?.groups?.count == 0
                    {
                        self.nearByPlacesView?.setEmptyList()
                    }
                    else
                    {
                        var places:[NearByPlaceDataModel] = []
                        
                        
                        for (_,group) in (response?.response?.groups?.enumerated())!
                            
                        {
                            for (_,item) in (group.items?.enumerated())!
                                
                            {
                                
                                places.append(NearByPlaceDataModel(name: (item.venue?.name)!, address: (item.venue?.location?.formattedAddress?.joined(separator: "\n"))!, imgUrl: "", id: (item.venue?.id)!))
                                
                            }
                            
                        }
                        
                        self.nearByPlacesView?.setPlaceList(places)
                        
                    }
                }
                else
                {
                
                    self.nearByPlacesView?.setError()

                }
                
                
            }
            else
            {
                self.nearByPlacesView?.setError()
            }
            
        }
    }
    
    
    
    public func getPlaceImage(photoId:String , index:Int){
        
        
        let result = Helper.getCurrentDate(format: "yyyyMMdd")
        
        
        exploreService.expolrePhoto(photo_id: photoId, version: result, client_id: getFoursquareKey().0, client_secret: getFoursquareKey().1) { (photoResponse, error) in
            
            if error == nil && photoResponse?.meta?.code == 200
            {
                
                if (photoResponse?.response?.photos?.items?.count)! > 0 {
                    
                    let imgUrl = (photoResponse?.response?.photos?.items?[0].prefix)! + Constants.imageSize +  (photoResponse?.response?.photos?.items?[0].suffix)!
                    
                    self.nearByPlacesView?.setPlaceImage(imgUrl: imgUrl , index: index)

                }
                
            }
           
        }
        
    }
    
    
    func getFoursquareKey() -> (String , String)
    {
        if let url = Bundle.main.url(forResource:"FourSquare", withExtension: "plist") // check keys for FourSquare from FourSquare.plist
        {
            do {
                let data = try Data(contentsOf:url)
                let swiftDictionary = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String:Any]
                let client_id = swiftDictionary["CLIENT_ID"] as! String
                let client_secret = swiftDictionary["CLIENT_SECRET"] as! String
                
                return (client_id , client_secret)
                
            } catch {
                print(error) // can not found keys
            }
        }
        
        return ("","")
    }
}
