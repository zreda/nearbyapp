//
//  PlaceTableViewcell.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class PlaceTableViewcell: UITableViewCell {
    
    @IBOutlet weak var placeImg: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeLocation: UILabel!
    
    
    
    /*
     
     update cell with data 
     
     */
    func configureCell(model:NearByPlaceDataModel)
    {
       placeName.text = model.name
       placeLocation.text = model.address
       placeImg.sd_setImage(with: URL(string: (model.imgUrl) ?? ""), placeholderImage: #imageLiteral(resourceName: "ic_photo"))

    }
}
