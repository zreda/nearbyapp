//
//  NearPlacesManager.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class NearPlacesManager: NSObject {
  
    
    /*
     
     get nearby places from FourSquare API
     
     */
    func expolreVenues(lat:String,lng:String,version:String , client_id:String , client_secret:String ,completion:  @escaping (_ :NearByResponseModel?,_ :NSError?)->Void)
    {
        
        ServiceProvider().sendUrl(showActivity:true,method: .get, URLString: String(format:Constants.Url.explore_url,lat,lng,client_id,client_secret,version), withQueryStringParameters: nil,  withHeaders: nil) { (response, error) in
            
            if error == nil
            {
                completion(NearByResponseModel(JSON:response as! [String:Any]), error)
            }
            else
            {
                completion(nil, error)

            }
            
        }
       
    }
    
    /*
     
     get image for each place from FourSquare API
     
     */
    
    func expolrePhoto(photo_id:String, version:String , client_id:String , client_secret:String ,completion:  @escaping (_ :PhotoResultModel?,_ :NSError?)->Void)
    {
        
        ServiceProvider().sendUrl(showActivity:false,method: .get, URLString: String(format:Constants.Url.photo_url,photo_id,client_id,client_secret,version), withQueryStringParameters: nil,  withHeaders: nil) { (response, error) in
            
            if error == nil
            {
                completion(PhotoResultModel(JSON:response as! [String:Any]), error)
            }
            else
            {
                completion(nil, error)
                
            }
            
        }
        
    }
   
    
}
