//
//  ServiceProvider.swift
//  Intercom
//
//  Created by Zeinab Reda on 3/11/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import Alamofire
import MBProgressHUD

class ServiceProvider {
    
    
    func sendUrl(showActivity:Bool,method:HTTPMethod,URLString: String, withQueryStringParameters parameters: [String : AnyObject]?, withHeaders headers: [String : String]?, completionHandler completion:@escaping (_ :NSObject?,_ :NSError?) -> Void)
    {
        
        var hud : MBProgressHUD = MBProgressHUD()
        
        if showActivity
        {
            hud = MBProgressHUD.showAdded(to: (Helper.getTopViewController()?.view) ?? UIView(), animated: true)
            hud.mode = .indeterminate
            hud.label.text = "Loading"
        }
        
        Alamofire.request(URLString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            debugPrint("Request: \(String(describing: response.request))")
            debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
            //            print("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
            hud.show(animated: true)
            
            debugPrint("Response: \(String(describing: response.result.value))")
            
            debugPrint("Error: \(String(describing: response.error))")
            
            
            switch(response.result) {
                
                
            case .success(_):
                hud.hide(animated: true)
                
                if response.response?.statusCode == 200
                {
                    if let data = response.result.value
                    {
                        completion(data as? NSObject ,nil)
                        
                    }
                }
                    
                else if response.response?.statusCode == Constants.StatusCode.StatusNotfound
                {
                    completion(NSObject() ,NSError(domain: "No Data Found", code: (response.response?.statusCode)! , userInfo: [:]))
                    
                    Helper.showFloatAlert(title: "No Data Found", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                    
                else if response.response?.statusCode == Constants.StatusCode.UserNotAuthorized
                {
                    
                    completion(NSObject() ,NSError(domain: "User Not Found", code: (response.response?.statusCode)! , userInfo: [:]))
                    
                }
                else
                {
                    completion(NSObject() ,NSError(domain: "No Internet Connection", code: (response.response?.statusCode)! , userInfo: [:]))
                    
                    //                    Helper.showFloatAlert(title: "Server Error , please try again later", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                
                break
                
            case .failure(_):
                hud.hide(animated: true)
                
                if response.response?.statusCode == Constants.StatusCode.StatusOK
                {
                    completion(NSObject() ,NSError(domain: "File Not Found", code: 0 , userInfo: [:]))
                    
                    
                    Helper.showFloatAlert(title: "File Not Found", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                    
                else if response.response?.statusCode == Constants.StatusCode.UserNotAuthorized || response.response?.statusCode == Constants.StatusCode.UserNotfound
                {
                    completion(NSObject() ,NSError(domain: "User Not Found", code: 0 , userInfo: [:]))
                    
                    Helper.showFloatAlert(title: "User Not Found", subTitle: "", type: Constants.AlertType.AlertError)
                    
                    
                }
                else
                {
                    completion(NSObject() ,NSError(domain: "No Internet Connection", code: 0 , userInfo: [:]))
                    
                    Helper.showFloatAlert(title: "No Internet Connection", subTitle: "", type: Constants.AlertType.AlertError)
                }
                
                break
                
            }
            
        }
        
    }
    
    
}
