//
//  NetworkUtil.swift
//  NearByApp
//
//  Created by Zeinab Reda on 9/29/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import ReachabilitySwift

class NetworkUtil: NSObject {
    
//    let reachability = Reachability()

    static func getNetworkStatus() -> Bool{
        
        let reachability  = Reachability.init(hostname: "www.google.com")
    
        if (reachability?.isReachable)! {
            return true
        }else{
            return false
        }
    }
}
