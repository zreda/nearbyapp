
import UIKit

let kInternetConnectionNotReachable: String = "NOT_REACHABLE"
let kInternetConnectionReachable: String = "REACHABLE"

class Constants: NSObject {
    
    let main_url:String = "https://api.foursquare.com/v2/"
    public static let imageSize:String = "100x100"
    
    public static let rowHeight: CGFloat = 100
    
    struct Url {
        
        static let explore_url = Constants().main_url+"venues/explore?ll=%@,%@&client_id=%@&client_secret=%@&v=%@"
        static let photo_url = Constants().main_url+"venues/%@/photos?client_id=%@&client_secret=%@&v=%@"

    }
    
    
    struct StatusCode {
        
        static let StatusOK = 200
        static let StatusNotfound = 400
        static let UserNotAuthorized = 401
        static let UserNotfound = 404
        
    }
    
    
    struct AlertType {
        static let AlertSuccess = 2
        static let AlertError = 1
        static let Alertinfo = 3
        static let AlertWarn = 4
    }
    
    struct userDefault {
        static let placesData = "placesData"
        static let locationMode = "locationMode"
    }
    
  
    
}
