//
//  Helper.swift
//
//  Created by Zeinab Reda on 11/15/16.
//  Copyright © 2016 Alaa Taher. All rights reserved.
//

import UIKit
import Foundation
import  JDropDownAlert


class Helper: NSObject {
    
    static let userDef = UserDefaults.standard
    
    static func convertDataToJson(data:Data) ->Any
    {
        var jsonDic : Any?
        do {
            jsonDic = try? JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions())
        }
        return jsonDic as Any
    }
    
 
    
    static func showFloatAlert(title:String ,subTitle:String ,type:Int){
        
        let alert = JDropDownAlert()
        alert.alertWith(title)

        if type == Constants.AlertType.AlertError
        {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.darkGray, backgroundColor: UIColor.red)
        }
        else if type == Constants.AlertType.AlertSuccess
        {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.darkGray, backgroundColor: UIColor.green)
            
        }
            
        else if type == Constants.AlertType.Alertinfo
        {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.darkGray, backgroundColor: UIColor.blue)
            
        }
        alert.didTapBlock = {
            debugPrint("Top View Did Tapped")
        }
    }
    
    
    static func getCurrentDate(format:String) -> String
    {
    
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let result = formatter.string(from: date)
        return result
    
    }
    
    
    // insert and retreive object or primitave data type in user defualt
    
    static func saveUserDefault(key:String,value:Any)
    {
        userDef.set(value, forKey: key)
    }
    static func getUserDefault(key:String)->Any
    {
        return userDef.object(forKey: key) as Any
    }
    static func removeKeyUserDefault(key:String)
    {
        return userDef.removeObject(forKey: key)
    }
    
    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return userDef.object(forKey: key) != nil
    }
    
    static func saveObjectDefault(key:String,value:Any)
    {
        let userDataEncoded = NSKeyedArchiver.archivedData(withRootObject: value)
        userDef.set(userDataEncoded, forKey: key)
        userDef.synchronize()
    }
    
    static func getObjectDefault(key:String)->Any
    {
        
        if let decodedNSData = UserDefaults.standard.object(forKey: key) as? NSData,
            let Data = NSKeyedUnarchiver.unarchiveObject(with: decodedNSData as Data)
        {
            
            return Data
        }
        else {
            debugPrint("Failed")
            
            return ""
        }
    }
    
    // Animate Table View
    static func animateTable(table:UITableView) {
        table.reloadData()
        
        let cells = table.visibleCells
        let tableHeight: CGFloat = table.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay:  0.05 * Double(index) , usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    static public func getTopViewController() -> UIViewController? {
        
        if var topViewController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topViewController.presentedViewController {
                topViewController = presentedViewController
            }
            
            return topViewController
        }
        
        return nil
    }
}

